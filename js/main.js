var ST = "#Stage_";
var S = "Stage_";
ivo.info({
    title: "Guia General",
    autor: "Edilson Laverde Molina",
    date: "",
    email: "edilsonlaverde_182@hotmail.com",
    icon: 'https://www.ucundinamarca.edu.co/images/iconos/favicon_verde_1.png',
    facebook: "https://www.facebook.com/edilson.laverde.molina"
});
var audios = [
    {
        url: "sonidos/click.mp3",
        name: "clic"
    },{
        url: "sonidos/bad.mp3",
        name: "bad"
    },{
        url: "sonidos/good.mp3",
        name: "good"
    }
];
const relation =[
    {
        dropp: "c-a",
        draggs:[
            "a",
            "d",
            "l"
        ],
    },{
        dropp: "c-d",
        draggs:[
            "a",
            "d",
            "l"
        ],
    },{
        dropp: "c-l",
        draggs:[
            "a",
            "d",
            "l"
        ],
    },{
        dropp: "c-f",
        draggs:[
            "f",
            "p",
            "i"
        ],
    },{
        dropp: "c-p",
        draggs:[
            "f",
            "p",
            "i"
        ],
    },{
        dropp: "c-i",
        draggs:[
            "f",
            "p",
            "i"
        ],
    },{
        dropp: "c-b",
        draggs:[
            "b"
        ]
    },
    {
        dropp: "c-m",
        draggs:[
            "m"
        ]
    },
    {
        dropp: "c-e",
        draggs:[
            "e"
        ]
    },
    {
        dropp: "c-n",
        draggs:[
            "n"
        ]
    },
    {
        dropp: "c-g",
        draggs:[
            "g",
            "r",
            "j"
        ]
    },
    {
        dropp: "c-r",
        draggs:[
            "g",
            "r",
            "j"
        ]
    },
    {
        dropp: "c-j",
        draggs:[
            "g",
            "r",
            "j"
        ]
    },
    {
        dropp: "c-c",
        draggs:[
            "c",
            "k",
            "s",
            "q",
            "h",
            "o"
        ]
    },
    {
        dropp: "c-k",
        draggs:[
            "c",
            "k",
            "s",
            "q",
            "h",
            "o"
        ]
    },
    {
        dropp: "c-s",
        draggs:[
            "c",
            "k",
            "s",
            "q",
            "h",
            "o"
        ]
    },
    {
        dropp: "c-s",
        draggs:[
            "c",
            "k",
            "s",
            "q",
            "h",
            "o"
        ]
    },
    {
        dropp: "c-q",
        draggs:[
            "c",
            "k",
            "s",
            "q",
            "h",
            "o"
        ]
    },
    {
        dropp: "c-h",
        draggs:[
            "c",
            "k",
            "s",
            "q",
            "h",
            "o"
        ]
    },
    ,
    {
        dropp: "c-o",
        draggs:[
            "c",
            "k",
            "s",
            "q",
            "h",
            "o"
        ]
    }
];
let points = 0;
let limit = 19;
function main(sym) {
    var t = null;
    udec = ivo.structure({
        created: function () {
            t = this;
            t.game_dragg_drop();
            t.events();
            t.animations();
            //precarga audios//
            ivo.load_audio(audios, onComplete = function () {
                ivo(ST + "preload").hide();
                stage1.play();
            });
        },
        methods: {
            game_dragg_drop: function () {
                $(".dragg").draggable({
                    revert: "invalid",
                    //centramos el mousel del dragg
                    cursor: "move",
                    cursorAt: {
                        top: 20,
                        left:54
                    },
                    start: function (event, ui) {
                        //ivo.play_audio("clic");
                    }
                });
                $(".dropp").droppable({
                    accept: ".dragg",
                    drop: function (event, ui) {
                        console.log(ui.draggable.attr("id"));
                        console.log($(this).attr("id"));
                        let dragg = ui.draggable.attr("id").split("Stage_")[1];
                        let dropp = $(this).attr("id").split("Stage_")[1];
                        let correct = false;
                        var _this = this;
                        
                        let {top, left} = $(this).position();
                        limit--;
                        relation.forEach(function (rel) {
                            if(rel.dropp == dropp){
                                rel.draggs.forEach(function (drag) {
                                    if(drag == dragg){
                                        correct = true;
                                    }
                                })
                            }
                            
                            //ponemos el dragg en el dropp
                            ui.draggable.css({
                                "top": top+"px",
                                "left": left+"px"
                            });
                            
                        });
                        if(correct){
                            points+=1;
                            console.log(points);
                            document.querySelector("#"+ui.draggable.attr("id")+" div:nth-of-type(1)").style.display = "block";
                            document.querySelector("#"+ui.draggable.attr("id")+" div:nth-of-type(2)").style.display = "none";
                            ivo.play("good");
                        }else{
                            document.querySelector("#"+ui.draggable.attr("id")+" div:nth-of-type(1)").style.display = "none";
                            document.querySelector("#"+ui.draggable.attr("id")+" div:nth-of-type(2)").style.display = "block";
                            ivo.play("bad");
                        }
                        if( limit == 0 ){
                            //fin del juego
                            let nota = (50 / 19) * points;
                            scorm = new MX_SCORM(false);
                            console.log("Estudiante "+scorm.info_user().name+" "+scorm.info_user().id+ " Nota "+nota+ " Puntos "+points);
                            scorm.set_score(nota);
                            if (nota>=30){
                                swal({
                                    title: "Buen trabajo!",
                                    text: "Felicitaciones!!",
                                    icon: "success",
                                  });
                            }else{
                                swal({
                                    title: "Lo sentimos!",
                                    text: "Vuelve a intentarlo!",
                                    icon: "error",
                                  });
                            }
                        }
                    }
                });

            },
            events: function () {

                var t = this;

                let btns = document.querySelectorAll(".dragg div:nth-of-type(1)");
                //los ocultamos
                btns.forEach(function (btn) {
                    btn.style.display = "none";
                });

                ivo(ST + "stage1").on("click", function () {
                    stage1.timeScale(3).reverse();
                    stage2.timeScale(1).play();
                    ivo.play("clic");
                });
                ivo(ST + "btninicio").on("click", function () {
                    stage2.timeScale(3).reverse();
                    stage1.timeScale(1).play();
                    ivo.play("clic");
                });

                let slider=ivo(ST+"slider").slider({
                    slides:'.slider',
                    btn_next:ST+"btn_sig",
                    btn_back:ST+"btn_atr",
                    rules:function(rule){
                        console.log("rules"+rule);
                    },
                    onMove:function(page){
                        ivo.play("clic");
                        
                    },
                    onFinish:function(){

                    }
                });


            },
            animations: function () {
                stage1 = new TimelineMax();
                stage1.append(TweenMax.from(ST + "stage1", .8, {x: 1300, opacity: 0}), 0);
                stage1.append(TweenMax.staggerFrom(ST + "stage1 div", .4, {x: 100, opacity: 0, scaleY: 6, ease: Elastic.easeOut.config(0.3, 0.4)}, .2), 0);
                stage1.stop();
                stage2 = new TimelineMax();
                stage2.append(TweenMax.from(ST + "stage2", .8, {x: 1300, opacity: 0}), 0);
                stage2.append(TweenMax.staggerFrom(".example", .4, {x: 100, opacity: 0, scaleY: 6, ease: Elastic.easeOut.config(0.3, 0.4)}, .2), 0);
                stage2.stop();
            }
        }
    });
}