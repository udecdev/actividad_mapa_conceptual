/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "both",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'stage1',
                            type: 'group',
                            rect: ['-8', '-22', '1032', '684', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Slide1Imagen',
                                type: 'image',
                                rect: ['0px', '0px', '547px', '684px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Slide1Imagen.png",'0px','0px']
                            },
                            {
                                id: 'Slide1Fondo',
                                type: 'image',
                                rect: ['492px', '0px', '540px', '684px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Slide1Fondo.png",'0px','0px']
                            },
                            {
                                id: 'Slide1Grafico',
                                type: 'image',
                                rect: ['537px', '155px', '108px', '107px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Slide1Grafico.png",'0px','0px']
                            },
                            {
                                id: 'Slide1Titulo',
                                type: 'image',
                                rect: ['537px', '306px', '349px', '184px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Slide1Titulo.png",'0px','0px']
                            }]
                        },
                        {
                            id: 'stage2',
                            type: 'group',
                            rect: ['0', '0', '1024', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'slider',
                                type: 'group',
                                rect: ['0', '0', '1024', '640', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'slider1',
                                    type: 'group',
                                    rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                    userClass: "slider",
                                    c: [
                                    {
                                        id: 'Slide2Grafico',
                                        type: 'image',
                                        rect: ['143px', '126px', '175px', '175px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"Slide2Grafico.png",'0px','0px']
                                    },
                                    {
                                        id: 'Slide2Grafico2',
                                        type: 'image',
                                        rect: ['633px', '428px', '86px', '87px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"Slide2Grafico2.png",'0px','0px']
                                    },
                                    {
                                        id: 't2',
                                        type: 'text',
                                        rect: ['251px', '423px', '359px', '70px', 'auto', 'auto'],
                                        text: "<p style=\"margin: 0px;\">​Recuerde que posee únicamente dos intentos para desarrollar esta actividad.</p>",
                                        align: "right",
                                        font: ['Arial, Helvetica, sans-serif', [26, "px"], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                                    },
                                    {
                                        id: 't3',
                                        type: 'text',
                                        rect: ['743px', '428px', '155px', '70px', 'auto', 'auto'],
                                        text: "<p style=\"margin: 0px; text-align: left;\">​¡Muchos</p><p style=\"margin: 0px; text-align: left;\">éxitos!</p>",
                                        align: "right",
                                        font: ['Arial, Helvetica, sans-serif', [37, "px"], "rgba(0,168,156,1.00)", "normal", "none", "", "break-word", "normal"]
                                    },
                                    {
                                        id: 't1',
                                        type: 'text',
                                        rect: ['350px', '130px', '425px', '70px', 'auto', 'auto'],
                                        text: "<p style=\"margin: 0px; text-align: left;\">​Ahora, le invitamos a complementar el siguiente esquema conceptual, basado en el concepto de Modelos de Gerencia.</p>",
                                        align: "right",
                                        font: ['Arial, Helvetica, sans-serif', [26, "px"], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                                    }]
                                },
                                {
                                    id: 'slider2',
                                    type: 'group',
                                    rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                    userClass: "slider",
                                    c: [
                                    {
                                        id: 'Slide3GraficoFondo',
                                        type: 'image',
                                        rect: ['366px', '108px', '535px', '270px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"Slide3GraficoFondo.png",'0px','0px']
                                    },
                                    {
                                        id: 'ts2-1',
                                        type: 'text',
                                        rect: ['68px', '162px', '219px', '142px', 'auto', 'auto'],
                                        text: "<p style=\"margin: 0px; line-height: 14px;\">​<span style=\"font-size: 14px;\">Para iniciar lo invitamos a que arrastre los modelos  de gerencia al espacio correspondiente para que complete el esquema&nbsp;</span></p><p style=\"margin: 0px; line-height: 14px;\"><span style=\"font-size: 14px;\">​</span></p><p style=\"margin: 0px; line-height: 14px;\"><span style=\"font-size: 14px;\">Tenga presente que en el centro empezaremos con el termino de Modelos Gerenciales&nbsp;</span></p>",
                                        font: ['Arial, Helvetica, sans-serif', [24, ""], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                                    },
                                    {
                                        id: 'ts2-2',
                                        type: 'text',
                                        rect: ['68px', '344px', '219px', '38px', 'auto', 'auto'],
                                        text: "<p style=\"margin: 0px; line-height: 14px;\">​!Buena Suerte!</p>",
                                        font: ['Arial, Helvetica, sans-serif', [24, ""], "rgba(0,168,156,1.00)", "normal", "none", "", "break-word", "normal"]
                                    },
                                    {
                                        id: 'Slide3Grafico',
                                        type: 'image',
                                        rect: ['68px', '73px', '78px', '70px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"Slide3Grafico.png",'0px','0px']
                                    },
                                    {
                                        id: 's',
                                        type: 'group',
                                        rect: ['781px', '492px', '86', '38', 'auto', 'auto'],
                                        userClass: "dragg",
                                        c: [
                                        {
                                            id: 'Slide3ss',
                                            type: 'image',
                                            rect: ['0px', '0px', '86px', '38px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Slide3ss.png",'0px','0px']
                                        },
                                        {
                                            id: 'Slide3s',
                                            type: 'image',
                                            rect: ['0px', '0px', '85px', '38px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Slide3s.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'r',
                                        type: 'group',
                                        rect: ['691px', '492px', '86', '38', 'auto', 'auto'],
                                        userClass: "dragg",
                                        c: [
                                        {
                                            id: 'Slide3rr',
                                            type: 'image',
                                            rect: ['0px', '0px', '85px', '38px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Slide3rr.png",'0px','0px']
                                        },
                                        {
                                            id: 'Slide3r',
                                            type: 'image',
                                            rect: ['0px', '0px', '86px', '38px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Slide3r.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'q',
                                        type: 'group',
                                        rect: ['601px', '492px', '85', '38', 'auto', 'auto'],
                                        userClass: "dragg",
                                        c: [
                                        {
                                            id: 'Slide3qq',
                                            type: 'image',
                                            rect: ['0px', '0px', '85px', '38px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Slide3qq.png",'0px','0px']
                                        },
                                        {
                                            id: 'Slide3q',
                                            type: 'image',
                                            rect: ['0px', '0px', '85px', '38px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Slide3q.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'p',
                                        type: 'group',
                                        rect: ['511px', '492px', '86', '38', 'auto', 'auto'],
                                        userClass: "dragg",
                                        c: [
                                        {
                                            id: 'Slide3pp',
                                            type: 'image',
                                            rect: ['0px', '0px', '86px', '38px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Slide3pp.png",'0px','0px']
                                        },
                                        {
                                            id: 'Slide3p',
                                            type: 'image',
                                            rect: ['0px', '0px', '85px', '38px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Slide3p.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'o',
                                        type: 'group',
                                        rect: ['421px', '492px', '86', '38', 'auto', 'auto'],
                                        userClass: "dragg",
                                        c: [
                                        {
                                            id: 'Slide3oo',
                                            type: 'image',
                                            rect: ['0px', '0px', '85px', '38px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Slide3oo.png",'0px','0px']
                                        },
                                        {
                                            id: 'Slide3o',
                                            type: 'image',
                                            rect: ['0px', '0px', '86px', '38px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Slide3o.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'n',
                                        type: 'group',
                                        rect: ['331px', '492px', '85', '38', 'auto', 'auto'],
                                        userClass: "dragg",
                                        c: [
                                        {
                                            id: 'Slide3nn',
                                            type: 'image',
                                            rect: ['0px', '0px', '85px', '38px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Slide3nn.png",'0px','0px']
                                        },
                                        {
                                            id: 'Slide3n',
                                            type: 'image',
                                            rect: ['0px', '0px', '85px', '38px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Slide3n.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'm',
                                        type: 'group',
                                        rect: ['241px', '492px', '86', '38', 'auto', 'auto'],
                                        userClass: "dragg",
                                        c: [
                                        {
                                            id: 'Slide3mm',
                                            type: 'image',
                                            rect: ['0px', '0px', '86px', '38px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Slide3mm.png",'0px','0px']
                                        },
                                        {
                                            id: 'Slide3m',
                                            type: 'image',
                                            rect: ['0px', '0px', '85px', '38px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Slide3m.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'l',
                                        type: 'group',
                                        rect: ['151px', '492px', '86', '38', 'auto', 'auto'],
                                        userClass: "dragg",
                                        c: [
                                        {
                                            id: 'Slide3ll',
                                            type: 'image',
                                            rect: ['0px', '0px', '85px', '38px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Slide3ll.png",'0px','0px']
                                        },
                                        {
                                            id: 'Slide3l',
                                            type: 'image',
                                            rect: ['0px', '0px', '86px', '38px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Slide3l.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'k',
                                        type: 'group',
                                        rect: ['61px', '492px', '85', '38', 'auto', 'auto'],
                                        userClass: "dragg",
                                        c: [
                                        {
                                            id: 'Slide3kk',
                                            type: 'image',
                                            rect: ['0px', '0px', '85px', '38px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Slide3kk.png",'0px','0px']
                                        },
                                        {
                                            id: 'Slide3k',
                                            type: 'image',
                                            rect: ['0px', '0px', '85px', '38px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Slide3k.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'j',
                                        type: 'group',
                                        rect: ['871px', '443px', '85', '39', 'auto', 'auto'],
                                        userClass: "dragg",
                                        c: [
                                        {
                                            id: 'Slide3jj',
                                            type: 'image',
                                            rect: ['0px', '0px', '85px', '39px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Slide3jj.png",'0px','0px']
                                        },
                                        {
                                            id: 'Slide3j',
                                            type: 'image',
                                            rect: ['0px', '0px', '85px', '38px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Slide3j.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'i',
                                        type: 'group',
                                        rect: ['781px', '443px', '85', '39', 'auto', 'auto'],
                                        userClass: "dragg",
                                        c: [
                                        {
                                            id: 'Slide3ii',
                                            type: 'image',
                                            rect: ['0px', '0px', '85px', '39px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Slide3ii.png",'0px','0px']
                                        },
                                        {
                                            id: 'Slide3i',
                                            type: 'image',
                                            rect: ['0px', '0px', '85px', '38px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Slide3i.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'h',
                                        type: 'group',
                                        rect: ['691px', '443px', '85', '39', 'auto', 'auto'],
                                        userClass: "dragg",
                                        c: [
                                        {
                                            id: 'Slide3hh',
                                            type: 'image',
                                            rect: ['0px', '0px', '85px', '39px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Slide3hh.png",'0px','0px']
                                        },
                                        {
                                            id: 'Slide3h',
                                            type: 'image',
                                            rect: ['0px', '0px', '85px', '38px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Slide3h.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'g',
                                        type: 'group',
                                        rect: ['601px', '443px', '85', '39', 'auto', 'auto'],
                                        userClass: "dragg",
                                        c: [
                                        {
                                            id: 'Slide3gg',
                                            type: 'image',
                                            rect: ['0px', '0px', '85px', '39px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Slide3gg.png",'0px','0px']
                                        },
                                        {
                                            id: 'Slide3g',
                                            type: 'image',
                                            rect: ['0px', '0px', '85px', '38px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Slide3g.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'f',
                                        type: 'group',
                                        rect: ['511px', '443px', '85', '39', 'auto', 'auto'],
                                        userClass: "dragg",
                                        c: [
                                        {
                                            id: 'Slide3ff',
                                            type: 'image',
                                            rect: ['0px', '0px', '85px', '39px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Slide3ff.png",'0px','0px']
                                        },
                                        {
                                            id: 'Slide3f',
                                            type: 'image',
                                            rect: ['0px', '0px', '85px', '38px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Slide3f.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'e',
                                        type: 'group',
                                        rect: ['421px', '443px', '85', '39', 'auto', 'auto'],
                                        userClass: "dragg",
                                        c: [
                                        {
                                            id: 'Slide3ee',
                                            type: 'image',
                                            rect: ['0px', '0px', '85px', '39px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Slide3ee.png",'0px','0px']
                                        },
                                        {
                                            id: 'Slide3e',
                                            type: 'image',
                                            rect: ['0px', '0px', '85px', '38px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Slide3e.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'd',
                                        type: 'group',
                                        rect: ['331px', '443px', '85', '39', 'auto', 'auto'],
                                        userClass: "dragg",
                                        c: [
                                        {
                                            id: 'Slide3dd',
                                            type: 'image',
                                            rect: ['0px', '0px', '85px', '39px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Slide3dd.png",'0px','0px']
                                        },
                                        {
                                            id: 'Slide3d',
                                            type: 'image',
                                            rect: ['0px', '0px', '85px', '38px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Slide3d.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'c',
                                        type: 'group',
                                        rect: ['241px', '443px', '85', '39', 'auto', 'auto'],
                                        userClass: "dragg",
                                        c: [
                                        {
                                            id: 'Slide3cc',
                                            type: 'image',
                                            rect: ['0px', '0px', '85px', '39px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Slide3cc.png",'0px','0px']
                                        },
                                        {
                                            id: 'Slide3c',
                                            type: 'image',
                                            rect: ['0px', '0px', '85px', '38px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Slide3c.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'b',
                                        type: 'group',
                                        rect: ['151px', '443px', '85', '39', 'auto', 'auto'],
                                        userClass: "dragg",
                                        c: [
                                        {
                                            id: 'Slide3bb',
                                            type: 'image',
                                            rect: ['0px', '0px', '85px', '39px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Slide3bb.png",'0px','0px']
                                        },
                                        {
                                            id: 'Slide3b',
                                            type: 'image',
                                            rect: ['0px', '0px', '85px', '38px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Slide3b.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'a',
                                        type: 'group',
                                        rect: ['61px', '443px', '85', '39', 'auto', 'auto'],
                                        userClass: "dragg",
                                        c: [
                                        {
                                            id: 'Slide3aa',
                                            type: 'image',
                                            rect: ['0px', '0px', '85px', '39px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Slide3aa.png",'0px','0px']
                                        },
                                        {
                                            id: 'Slide3a',
                                            type: 'image',
                                            rect: ['0px', '0px', '85px', '38px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Slide3a.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'c-a',
                                        type: 'rect',
                                        rect: ['366px', '108px', '85px', '39px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0.00)"],
                                        stroke: [0,"rgba(0,0,0,1)","none"],
                                        userClass: "dropp"
                                    },
                                    {
                                        id: 'c-b',
                                        type: 'rect',
                                        rect: ['491px', '158px', '85px', '39px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0.00)"],
                                        stroke: [0,"rgba(0,0,0,1)","none"],
                                        userClass: "dropp"
                                    },
                                    {
                                        id: 'c-c',
                                        type: 'rect',
                                        rect: ['816px', '109px', '85px', '39px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0.00)"],
                                        stroke: [0,"rgba(0,0,0,1)","none"],
                                        userClass: "dropp"
                                    },
                                    {
                                        id: 'c-d',
                                        type: 'rect',
                                        rect: ['365px', '155px', '85px', '39px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0.00)"],
                                        stroke: [0,"rgba(0,0,0,1)","none"],
                                        userClass: "dropp"
                                    },
                                    {
                                        id: 'c-e',
                                        type: 'rect',
                                        rect: ['591px', '158px', '85px', '39px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0.00)"],
                                        stroke: [0,"rgba(0,0,0,1)","none"],
                                        userClass: "dropp"
                                    },
                                    {
                                        id: 'c-f',
                                        type: 'rect',
                                        rect: ['365px', '247px', '85px', '39px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0.00)"],
                                        stroke: [0,"rgba(0,0,0,1)","none"],
                                        userClass: "dropp"
                                    },
                                    {
                                        id: 'c-g',
                                        type: 'rect',
                                        rect: ['491px', '338px', '85px', '39px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0.00)"],
                                        stroke: [0,"rgba(0,0,0,1)","none"],
                                        userClass: "dropp"
                                    },
                                    {
                                        id: 'c-h',
                                        type: 'rect',
                                        rect: ['816px', '338px', '85px', '39px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0.00)"],
                                        stroke: [0,"rgba(0,0,0,1)","none"],
                                        userClass: "dropp"
                                    },
                                    {
                                        id: 'c-i',
                                        type: 'rect',
                                        rect: ['365px', '338px', '85px', '39px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0.00)"],
                                        stroke: [0,"rgba(0,0,0,1)","none"],
                                        userClass: "dropp"
                                    },
                                    {
                                        id: 'c-j',
                                        type: 'rect',
                                        rect: ['691px', '338px', '85px', '39px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0.00)"],
                                        stroke: [0,"rgba(0,0,0,1)","none"],
                                        userClass: "dropp"
                                    },
                                    {
                                        id: 'c-k',
                                        type: 'rect',
                                        rect: ['816px', '155px', '85px', '39px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0.00)"],
                                        stroke: [0,"rgba(0,0,0,1)","none"],
                                        userClass: "dropp"
                                    },
                                    {
                                        id: 'c-l',
                                        type: 'rect',
                                        rect: ['366px', '201px', '85px', '39px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0.00)"],
                                        stroke: [0,"rgba(0,0,0,1)","none"],
                                        userClass: "dropp"
                                    },
                                    {
                                        id: 'c-m',
                                        type: 'rect',
                                        rect: ['690px', '158px', '85px', '39px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0.00)"],
                                        stroke: [0,"rgba(0,0,0,1)","none"],
                                        userClass: "dropp"
                                    },
                                    {
                                        id: 'c-n',
                                        type: 'rect',
                                        rect: ['590px', '249px', '85px', '39px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0.00)"],
                                        stroke: [0,"rgba(0,0,0,1)","none"],
                                        userClass: "dropp"
                                    },
                                    {
                                        id: 'c-o',
                                        type: 'rect',
                                        rect: ['816px', '201px', '85px', '39px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0.00)"],
                                        stroke: [0,"rgba(0,0,0,1)","none"],
                                        userClass: "dropp"
                                    },
                                    {
                                        id: 'c-p',
                                        type: 'rect',
                                        rect: ['366px', '293px', '85px', '39px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0.00)"],
                                        stroke: [0,"rgba(0,0,0,1)","none"],
                                        userClass: "dropp"
                                    },
                                    {
                                        id: 'c-q',
                                        type: 'rect',
                                        rect: ['816px', '292px', '85px', '39px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0.00)"],
                                        stroke: [0,"rgba(0,0,0,1)","none"],
                                        userClass: "dropp"
                                    },
                                    {
                                        id: 'c-r',
                                        type: 'rect',
                                        rect: ['592px', '338px', '85px', '39px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0.00)"],
                                        stroke: [0,"rgba(0,0,0,1)","none"],
                                        userClass: "dropp"
                                    },
                                    {
                                        id: 'c-s',
                                        type: 'rect',
                                        rect: ['816px', '247px', '85px', '39px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0.00)"],
                                        stroke: [0,"rgba(0,0,0,1)","none"],
                                        userClass: "dropp"
                                    }]
                                }]
                            },
                            {
                                id: 'btninicio',
                                type: 'image',
                                rect: ['477px', '582px', '104px', '43px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"btninicio.png",'0px','0px']
                            },
                            {
                                id: 'btn_atr',
                                type: 'image',
                                rect: ['366px', '582px', '105px', '43px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"btnatrashover.png",'0px','0px']
                            },
                            {
                                id: 'btn_sig',
                                type: 'image',
                                rect: ['587px', '582px', '104px', '42px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"btnsiguientehover.png",'0px','0px']
                            }]
                        },
                        {
                            id: 'preload',
                            type: 'group',
                            rect: ['0', '0', '1024', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'fondo-preload',
                                type: 'rect',
                                rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,1)"],
                                stroke: [0,"rgb(0, 0, 0)","none"]
                            },
                            {
                                id: 'logo',
                                type: 'image',
                                rect: ['462px', '270px', '100px', '100px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"logo.svg",'0px','0px']
                            }]
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1024px', '640px', 'auto', 'auto'],
                            overflow: 'hidden',
                            fill: ["rgba(255,255,255,1)"]
                        }
                    }
                },
                timeline: {
                    duration: 0,
                    autoPlay: true,
                    data: [

                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("index_edgeActions.js");
})("EDGE-247297");
